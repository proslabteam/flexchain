# Flexchain

In this repository are stored:

 - the executable jar of the **user interface** 
 - the executable jar of the **off-chain processor**
 - the Solidity file of the smart contracts for the process tempalte and the watcher
 - the bpmn files of the case study

To test the Flexchain prototype it is necessary to launch both the interface and the processor.  


## Execute the User interface
Once downloaded, open your terminal and start the interface using 

// Java 1.8  
  `java -jar Flexchain_userinterface.jar` 

// Java 9+    
 `java --module-path /USER_JavaFX_path/lib --add-modules=javafx.controls,javafx.fxml -jar Flexchain_userinterface.jar`

At this point, the GUI appears and all the functionalities can be invoked.

**Load the flight departure case study**: insert the smart contract address `0x533a5542f7317F86Ebb3d2B04D21627F5383beAD` in the form and click *Set Contract Address*. Now the contract is loaded and it is possible to interact with it.

**Get the state and the rule of the application message**: to check both the rule and the state of the *application* message, insert its id `Message_0flb950` in the form and select both *Get Rule* and *Get Message State*, the related information retrieved from the blockchain will appear below.

**Get the value of the attendee_identification variable**: to get the content of the attendee_identification from the blockchain, insert `attendee_identification` in the form, select *String* as type and click *Get Variable*, you will see the value previously exchanged.

**Deploy of the model**: for a new deployment instead, load the `career_upgrade.bpmn` file in the form and click *Load Custom Model*, the generatedcontract address will appear. In this case the processor is not mandatory for the succesfully deploy of the contract but it is required for starting to listen the next events.  

**Update of the model**: to update the flight departure model, uplaod the `career_upgrade_updated.bpmn` in the form and click *Update Model*, it will produce the transactions for adding, removing or updating the rules. 

**Execute the application message**:  to send the application message, insert its id `Message_0flb950` in the second form, then provide two input separated by a comma and click *Execute Message*. If the processor is already running, after a couple of minutes you should see the updated value of the varaible and the state of the message by using the interface.

To track the transactions of the new models it is possible to use [Etherscan](https://rinkeby.etherscan.io/) that will show all the information related to the inserted contract address.

## Execute the Off-chain processor

To start the execution of the processor, open a terminal and write
 `java -jar FlexChain_Processor.jar`
At this point the processor will be created and it will start to listen for new events.

In this case, the user has not active role since the processor will automatically react depending on the event type.
